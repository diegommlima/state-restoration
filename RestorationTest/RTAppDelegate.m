//
//  RTAppDelegate.m
//  RestorationTest
//
//  Created by Diego Lima on 08/08/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import "RTAppDelegate.h"
#import "RTFirstTableViewController.h"

@implementation RTAppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self commonInitWithOptions:launchOptions];
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self commonInitWithOptions:launchOptions];
    return YES;
}

- (void)commonInitWithOptions:(NSDictionary *)launchOptions {
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^ {
        
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:[RTFirstTableViewController new]];
        nav.restorationIdentifier = NSStringFromClass([UINavigationController class]);
        self.window.rootViewController = nav;
        [self.window makeKeyAndVisible];
    });
}

#pragma mark - Restoration

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

- (void)application:(UIApplication *)application didDecodeRestorableStateWithCoder:(NSCoder *)coder
{
    [[UIApplication sharedApplication] extendStateRestoration];
    
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(globalQueue, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[UIApplication sharedApplication] completeStateRestoration];
        });
    });
}

@end
