//
//  RTFirstTableViewController.m
//  RestorationTest
//
//  Created by Diego Lima on 08/08/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import "RTFirstTableViewController.h"
#import "RTSecondTableViewController.h"

@interface RTFirstTableViewController () <UIDataSourceModelAssociation, UIViewControllerRestoration>

@end

static NSString *kCellIdentifier = @"CellIdentifier";
static NSString *kUnsavedEditStateKey = @"unsavedEditStateKey";
static NSString *kObjectStateKey = @"ObjectStateKey";
static NSString *kViewControllerRestorationIdentifier = @"RTFirstTableViewController";
static NSString *kTableViewRestorationIdentifier = @"TableViewIdentifer";

@implementation RTFirstTableViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.restorationIdentifier = kViewControllerRestorationIdentifier;
        self.restorationClass = [self class];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"FIRST";

    self.tableView.restorationIdentifier = kTableViewRestorationIdentifier;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 15;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"First: %d",indexPath.row];
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    RTSecondTableViewController *second = [[RTSecondTableViewController alloc] init];
    second.title = [NSString stringWithFormat:@"Second - %d",indexPath.row];
    [self.navigationController pushViewController:second animated:YES];
}

#pragma mark - UIStateRestoring

// this is called when the app is suspended to the background
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];

    [coder encodeBool:[self.tableView isEditing] forKey:kUnsavedEditStateKey];
    [coder encodeObject:@"valor" forKey:kObjectStateKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
    
    self.tableView.editing = [coder decodeBoolForKey:kUnsavedEditStateKey];

    [self.tableView reloadData];
}

#pragma mark - UIDataSourceModelAssociation

- (NSString *)modelIdentifierForElementAtIndexPath:(NSIndexPath *)path inView:(UIView *)view
{
    
    NSString *identifier = [NSString stringWithFormat:@"PATH:%d",path.row];
    
    return identifier;
}

- (NSIndexPath *)indexPathForElementWithModelIdentifier:(NSString *)identifier inView:(UIView *)view
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:10 inSection:0];

    [self.tableView reloadData];
    
    return indexPath;
}

#pragma mark - UIViewControllerRestoration Method

+ (UIViewController *)viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{

    if ([[identifierComponents lastObject] isEqualToString:kViewControllerRestorationIdentifier]) {
        
        RTFirstTableViewController *vc = [[RTFirstTableViewController alloc] init];
        return vc;
    }
    return nil;
}

@end
