//
//  RTAppDelegate.h
//  RestorationTest
//
//  Created by Diego Lima on 08/08/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
