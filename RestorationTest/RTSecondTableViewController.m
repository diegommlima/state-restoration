//
//  RTSecondTableViewController.m
//  RestorationTest
//
//  Created by Diego Lima on 08/08/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import "RTSecondTableViewController.h"
#import "RTLastViewController.h"

@interface RTSecondTableViewController () <UIDataSourceModelAssociation, UIViewControllerRestoration>

@end

static NSString *kCellIdentifier = @"CellIdentifier";
static NSString *kTableViewRestorationIdentifier = @"TableViewIdentifer";
static NSString *kTitleStateKey = @"TitleStateKey";

@implementation RTSecondTableViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        self.restorationClass = [self class];
        self.restorationIdentifier = NSStringFromClass([self class]);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.restorationIdentifier = NSStringFromClass([self.tableView class]);
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 15;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"Second: %d",indexPath.row];
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    RTLastViewController *lastVC = [[RTLastViewController alloc] init];
    lastVC.title = [NSString stringWithFormat:@"Last - %d",indexPath.row];
    [self.navigationController pushViewController:lastVC animated:YES];
}

#pragma mark - UIStateRestoring

// this is called when the app is suspended to the background
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
    
    [coder encodeObject:self.title forKey:kTitleStateKey];
}

// this is called when the app is re-launched
- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
    
    self.title = [coder decodeObjectForKey:kTitleStateKey];

    [self.tableView reloadData];
}

#pragma mark - UIDataSourceModelAssociation

- (NSString *)modelIdentifierForElementAtIndexPath:(NSIndexPath *)path inView:(UIView *)view
{
    
    NSString *identifier = [NSString stringWithFormat:@"PATH:%d",path.row];
    
    return identifier;
}

- (NSIndexPath *)indexPathForElementWithModelIdentifier:(NSString *)identifier inView:(UIView *)view
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:10 inSection:0];
    
    [self.tableView reloadData];
    
    return indexPath;
}

#pragma mark - UIViewControllerRestoration Method

+ (UIViewController *)viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{    
    if ([[identifierComponents lastObject] isEqualToString:NSStringFromClass([self class])]) {
        
        RTSecondTableViewController *vc = [[RTSecondTableViewController alloc] init];
        return vc;
    }
    
    return nil;
}

@end
