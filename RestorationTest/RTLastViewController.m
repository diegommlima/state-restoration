//
//  RTLastViewController.m
//  RestorationTest
//
//  Created by Diego Lima on 08/08/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import "RTLastViewController.h"

@interface RTLastViewController () <UIViewControllerRestoration>

@end

static NSString *kTitleStateKey = @"TitleStateKey";
static NSString *kColorStateKey = @"ColorStateKey";

@implementation RTLastViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        self.restorationClass = [self class];
        self.restorationIdentifier = NSStringFromClass([self class]);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGFloat red = arc4random_uniform(255) / 255.0;
    CGFloat green = arc4random_uniform(255) / 255.0;
    CGFloat blue = arc4random_uniform(255) / 255.0;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    self.view.backgroundColor = color;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIStateRestoring

// this is called when the app is suspended to the background
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
    
    [coder encodeObject:self.title forKey:kTitleStateKey];
    [coder encodeObject:self.view.backgroundColor forKey:kColorStateKey];

}

// this is called when the app is re-launched
- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
    
    self.title = [coder decodeObjectForKey:kTitleStateKey];
    self.view.backgroundColor = [coder decodeObjectForKey:kColorStateKey];
}

#pragma mark - UIViewControllerRestoration Method

+ (UIViewController *)viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
    
    if ([[identifierComponents lastObject] isEqualToString:NSStringFromClass([self class])]) {

        RTLastViewController *vc = [[RTLastViewController alloc] init];
        return vc;
    }
    
    return nil;
}

@end
