//
//  main.m
//  RestorationTest
//
//  Created by Diego Lima on 08/08/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RTAppDelegate class]));
    }
}
